function doIt(){
    let num1Ref = document.getElementById('number1');
    let num1 = Number(num1Ref.value);
    let num2Ref = document.getElementById('number2');
    let num2 = Number(num2Ref.value);
    let num3Ref = document.getElementById('number3');
    let num3 = Number(num3Ref.value);
    let ansRef = document.getElementById('answer');
    let ans = num1+num2+num3;
    ansRef.innerText = ans;
    
    if (ans >= 0){
        ansRef.className = 'positive';
    }
    else{
        ansRef.className = 'negative';
    }
    
    oddevenRef = document.getElementById('oddeven')
    
    if (ans %2 === 0){
        oddevenRef.innerText='(even)';
        oddevenRef.className='even';
    }
    else{
        oddevenRef.innerText='(odd)';
        oddevenRef.className='odd';
    }
}