//question 1 
var r=4; 
circumference=2*r*Math.PI; 
console.log(circumference.toFixed(2)); 
 
//question 2 
let animalString="cameldogcatlizard"; 
let andString=" and "; 
console.log(animalString.substr(8,3)+andString+animalString.substr(5,3)); //substr 
console.log(animalString.substring(8,11)+andString+animalString.substring(5,8)); //substring*/ 
 
//question 3 
var person={ 
  firstName:"Kanye", 
  lastName:"West", 
  birthDate:"8 June 1977", 
  annualIncome:150000000.00 
} 
console.log(person.firstName+' '+person.lastName+' was born on '+person.birthDate 
            +' and has an annual income of $'+person.annualIncome) 
 
//question 4 
var number1, number2; 
//RHS generates a random number between 1 and 10 inclusive 
number1 = Math.floor((Math.random() * 10) + 1); 
//RHS generates a random number between 1 and 10 inclusive 
number2 = Math.floor((Math.random() * 10) + 1); 
console.log("number1 = " + number1 + " number2 = " + number2); 
//HERE your code to swap the values in number1 and number2 
var number3=0; 
number3=number1; 
number1=number2 
number2=number3 
console.log("number1 = " + number1 + " number2 = " + number2); 
 
//question 5 
let year; 
let yearNot2015Or2016; 
year = 2016; 
yearNot2015Or2016 = year !== 2015 && year !== 2016; 
console.log(yearNot2015Or2016);